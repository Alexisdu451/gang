package fr.alexis.gang.gestion.gui;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class GangGUI  {

    public static void createGangGUI(Player player){
        Inventory gui = Bukkit.createInventory(player, 9, "§6Les Bolosses");

        ItemStack buisness = new ItemStack(Material.GOLD_INGOT);
        ItemStack gestion = new ItemStack(Material.ITEM_FRAME);

        ItemMeta buisnessItemMeta = buisness.getItemMeta();
        buisnessItemMeta.setDisplayName("§6Buisness");
        ArrayList<String> buisnessLore = new ArrayList<>();
        buisnessLore.add("§bPermet de gerer son Buisness");
        buisnessItemMeta.setLore(buisnessLore);
        buisness.setItemMeta(buisnessItemMeta);

        ItemMeta gestItemMeta = gestion.getItemMeta();
        gestItemMeta.setDisplayName("§6Gestion du Gang");
        ArrayList<String> gestLore = new ArrayList<>();
        gestLore.add("§bPermet de gerer ton gang");
        gestItemMeta.setLore(gestLore);
        gestion.setItemMeta(gestItemMeta);

        ItemStack[] menuItems = {buisness, gestion};
        gui.setContents(menuItems);
        player.openInventory(gui);
    }

}
