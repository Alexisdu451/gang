package fr.alexis.gang.gestion.gui.commands;

import fr.alexis.gang.gestion.gui.GangGUI;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GUICommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        Player player = (Player) commandSender;
        GangGUI.createGangGUI(player);

        return false;
    }
}
