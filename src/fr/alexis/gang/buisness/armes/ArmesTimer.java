package fr.alexis.gang.buisness.armes;

import fr.alexis.gang.Gang;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.scheduler.BukkitTask;

public class ArmesTimer {
    public static BukkitTask task;
    public static int count;

    public static void startTimer(Player player) {
        count = 5;
        task = Bukkit.getScheduler().runTaskTimer(Gang.getVendeurArmes(), () -> {
            if (count == 0) {
                task.cancel();
                Caisse.largageCommande(player);
                player.sendMessage("§4Vendeur Armes §6» §bCommande larger!");
            } else if (count > 0)
                player.sendMessage("§4Vendeur Armes §6» §bIl reste " + count + " seconds avant le largage!");
            count--;
        }, 20, 20);
    }

    public static void startTimerFire(Location location) {
        task = Bukkit.getScheduler().runTaskTimer(Gang.getVendeurArmes(), () -> {
            Location loc = location;
            Firework firework = (Firework) loc.getWorld().spawnEntity(loc, EntityType.FIREWORK);
            FireworkMeta fireworkMeta = firework.getFireworkMeta();

            fireworkMeta.setPower(2);
            fireworkMeta.addEffect(FireworkEffect.builder().withColor(Color.LIME).flicker(true).build());

            firework.setFireworkMeta(fireworkMeta);
            firework.detonate();

            Firework firework1 = (Firework) loc.getWorld().spawnEntity(loc, EntityType.FIREWORK);
            firework1.setFireworkMeta(fireworkMeta);

        }, 20, 20);

    }


}





