package fr.alexis.gang.buisness.armes.commands;

import fr.alexis.gang.buisness.armes.Caisse;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TestCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        Player player = (Player) commandSender;
        Caisse.createCommande(player);

        return false;
    }
}
