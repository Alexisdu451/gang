package fr.alexis.gang.buisness.armes.commands;

import fr.alexis.gang.buisness.armes.Caisse;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AddCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        Player player = (Player) commandSender;
        int pos = Integer.parseInt(strings[0]);
        String itemStack = strings[1];
        Caisse.addCommande(itemStack, pos, player);

        return false;
    }
}

