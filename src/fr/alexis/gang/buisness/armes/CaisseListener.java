package fr.alexis.gang.buisness.armes;

import fr.alexis.gang.buisness.armes.gui.ArmesGUIListener;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class CaisseListener implements Listener {

    @EventHandler
    public void onClick(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && (event.getClickedBlock().getType() == Material.ENDER_CHEST)) {
            event.setCancelled(true);
            player.sendMessage("§4Vendeur Armes §6» §bVous venez de récuperer votre commande!");
            for (int i = 0; i < Caisse.Commande.size(); i++) {
                player.getInventory().addItem(new ItemStack(Material.getMaterial(Caisse.Commande.get(i)), 1));
            }
            event.getClickedBlock().setType(Material.AIR);
            ArmesTimer.task.cancel();
            ArmesGUIListener.i = -1;
            Caisse.commande_status.put(0, true);
        }

    }
}
