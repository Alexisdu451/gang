package fr.alexis.gang.buisness.armes.gui;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class CommandesArmesGUI {
    public static void createCommandesArmesGUI(Player player){
        Inventory gui = Bukkit.createInventory(player, 9, "§bCommande d'Armes");

        ItemStack caisse = new ItemStack(Material.END_CRYSTAL);
        ItemStack gilet = new ItemStack(Material.DIAMOND_CHESTPLATE);

        ItemMeta caisseItemMeta = caisse.getItemMeta();
        caisseItemMeta.setDisplayName("§6Caisse d'armes");
        ArrayList<String> caisseLore = new ArrayList<>();
        caisseLore.add("§bPrix » 300€");
        caisseItemMeta.setLore(caisseLore);
        caisse.setItemMeta(caisseItemMeta);

        ItemMeta giletItemMeta = gilet.getItemMeta();
        giletItemMeta.setDisplayName("§6Gilet");
        ArrayList<String> giletLore = new ArrayList<>();
        giletLore.add("§bPrix » 500€");
        giletItemMeta.setLore(giletLore);
        gilet.setItemMeta(giletItemMeta);

        ItemStack[] menuItems = {caisse, gilet};
        gui.setContents(menuItems);
        player.openInventory(gui);
    }
}
