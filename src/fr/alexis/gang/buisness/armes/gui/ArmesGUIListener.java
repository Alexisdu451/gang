package fr.alexis.gang.buisness.armes.gui;

import fr.alexis.gang.buisness.armes.Caisse;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class ArmesGUIListener implements Listener {

    public static Integer i = -1;

    @EventHandler
    public void clickEvent(InventoryClickEvent e) {
        if (e.getView().getTitle().equals("§bVendeur d'Armes")) {
            Player player = (Player) e.getWhoClicked();
            switch (e.getCurrentItem().getType()) {
                case ENDER_CHEST:
                    player.closeInventory();
                    CommandesArmesGUI.createCommandesArmesGUI(player);
                    break;

                case END_CRYSTAL:
                    Caisse.venteCaisse(player);
                    break;

            }

            e.setCancelled(true);

        }
        if (e.getView().getTitle().equals("§bCommande d'Armes")) {
            Player player = (Player) e.getWhoClicked();
            switch (e.getCurrentItem().getType()) {
                case END_CRYSTAL:
                    i+=1;
                    player.closeInventory();
                    Caisse.addCommande("END_CRYSTAL", i, player);
                    break;

                case DIAMOND_CHESTPLATE:
                    i+=1;
                    player.closeInventory();
                    Caisse.addCommande("DIAMOND_CHESTPLATE", i, player);
                    break;
            }

            e.setCancelled(true);

        }
    }
}
