package fr.alexis.gang.buisness.armes.gui;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class ArmesGUI {
    public static void createArmesGUI(Player player){
        Inventory gui = Bukkit.createInventory(player, 9, "§bVendeur d'Armes");

        ItemStack commande = new ItemStack(Material.ENDER_CHEST);
        ItemStack vente = new ItemStack(Material.END_CRYSTAL);

        ItemMeta commandeItemMeta = commande.getItemMeta();
        commandeItemMeta.setDisplayName("§6Commande");
        ArrayList<String> commandeLore = new ArrayList<>();
        commandeLore.add("§bPour commander des armes");
        commandeItemMeta.setLore(commandeLore);
        commande.setItemMeta(commandeItemMeta);

        ItemMeta venteItemMeta = vente.getItemMeta();
        venteItemMeta.setDisplayName("§6Vendre les caisses d'armes");
        ArrayList<String> venteLore = new ArrayList<>();
        venteLore.add("§b5000€ par caisse");
        venteItemMeta.setLore(venteLore);
        vente.setItemMeta(venteItemMeta);

        ItemStack[] menuItems = {commande, vente};
        gui.setContents(menuItems);
        player.openInventory(gui);
    }
}
