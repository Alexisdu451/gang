package fr.alexis.gang.buisness.armes;

import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Caisse {
    //public List<String> Commande = new ArrayList<String>();
    public static Map<Integer, String> Commande = new HashMap<Integer, String>();
    public static Map<Integer, Boolean> commande_status = new HashMap<Integer, Boolean>();
    public static Map<Integer, String> position = new HashMap<Integer, String>();


    public static void createCommande(Player player) {
        if (commande_status.get(0) && !Commande.isEmpty()) {
            commande_status.put(0, false);
            Bukkit.broadcastMessage(String.valueOf(commande_status));
            player.sendMessage(String.valueOf(Commande.get(0)));
            player.sendMessage("§4Vendeur Armes §6» §bVous venez de passer une commande");
            ArmesTimer.startTimer(player);
        } else
            player.sendMessage("§4Vendeur Armes §6» §bUne commande est déjà en cours");


    }
    public static void venteCaisse(Player player){
        if(player.getInventory().getItemInHand().getType().equals(Material.END_CRYSTAL)){
            int count = player.getInventory().getItemInMainHand().getAmount();
            player.getInventory().removeItem(new ItemStack(Material.END_CRYSTAL, count));
            player.sendMessage("§4Vendeur Armes §6» §bVous venez de vendre " + count + " caisse d'armes");
        } else
            player.sendMessage("§4Vendeur Armes §6» §bVous n'avez pas de caisse d'armes à vendre!");

    }


    public static void largageCommande(Player player) {
        Random random = new Random();
        int max = position.size();
        int min = 0;
        int rand = min + random.nextInt(max - min);
        Bukkit.broadcastMessage(String.valueOf(rand));
        String[] parts = position.get(rand).split("#");
        Location loc = new Location(Bukkit.getWorld("world"), Double.parseDouble(parts[0]), Double.parseDouble(parts[1]), Double.parseDouble(parts[2]));
        loc.getBlock().setType(Material.ENDER_CHEST);
        ArmesTimer.startTimerFire(loc);
        player.sendMessage("§4Vendeur Armes §6» §bVotre commande à été larguer en  x=" + Double.parseDouble(parts[0]) + " y=" + Double.parseDouble(parts[1]) + " z=" + Double.parseDouble(parts[2]));
    }


    public static void addCommande(String items, int pos, Player player) {
        Bukkit.broadcastMessage(String.valueOf(commande_status.get(0)));
        if (commande_status.get(0)) {
            Commande.put(pos, items);
            Bukkit.broadcastMessage(String.valueOf(pos));
            player.sendMessage("§4Vendeur Armes §6» §bVous venez d'ajouter " + items + " à votre commande!");
        } else if (!commande_status.get(0)) player.sendMessage("§4Vendeur Armes §6» §bUne commande est déjà en cours!");


    }

}
