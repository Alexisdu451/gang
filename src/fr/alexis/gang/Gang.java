package fr.alexis.gang;

import fr.alexis.gang.buisness.SetupBuisness;
import fr.alexis.gang.buisness.armes.commands.AddCommand;
import fr.alexis.gang.buisness.armes.CaisseListener;
import fr.alexis.gang.buisness.armes.commands.TestCommand;
import fr.alexis.gang.buisness.armes.gui.ArmesGUIListener;
import fr.alexis.gang.buisness.armes.gui.CommandesArmesGUI;
import fr.alexis.gang.gestion.gui.GangGUIListener;
import fr.alexis.gang.gestion.gui.commands.GUICommand;
import org.bukkit.plugin.java.JavaPlugin;

public class Gang extends JavaPlugin {
    public static Gang gang;

    @Override
    public void onEnable() {
        gang = this;
        SetupBuisness.setupArmes();
        this.getCommand("test").setExecutor(new TestCommand());
        this.getCommand("add").setExecutor(new AddCommand());
        this.getCommand("gui").setExecutor(new GUICommand());
        this.getServer().getPluginManager().registerEvents(new CaisseListener(), this);
        this.getServer().getPluginManager().registerEvents(new GangGUIListener(), this);
        this.getServer().getPluginManager().registerEvents(new ArmesGUIListener(), this);


    }

    public static Gang getVendeurArmes() {
        return gang;
    }
}
